package pdf_android.air.pdftest.file_module;

import android.annotation.SuppressLint;

import java.io.File;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import pdf_android.air.pdftest.model.FileEntity;

public class DirectoryReader {

    private String rootDirectory;

    DirectoryReader(String path) {
        rootDirectory = path;
    }

    boolean fileExists(){
        File file = new File(rootDirectory);
        return file.exists() || file.isDirectory();
    }

    boolean folderExist(String path){
        return !new File(rootDirectory).exists();
    }

    public ArrayList<FileEntity> getFilesList() {
        ArrayList<FileEntity> fileList = new ArrayList<>();
        File folder = new File(rootDirectory);
        if(folderExist(rootDirectory)){
            new File(rootDirectory).mkdirs();
        } else {
            if(folder.listFiles() != null) {
                for (int i = 0; i < folder.listFiles().length; i++) {
                    if (folder.listFiles()[i].isFile()) {
                        FileEntity fileEntity = new FileEntity();
                        fileEntity.nameFile = folder.listFiles()[i].getName();
                        File file = new File(rootDirectory + "/" + folder.listFiles()[i].getName());
                        fileEntity.dateCreate = dateCreate(file);
                        DecimalFormat df = new DecimalFormat("#.##");
                        fileEntity.sizeFile = df.format(sizeFile(file));
                        fileEntity.pathToFile = file.getAbsolutePath();
                        fileList.add(fileEntity);
                    }
                }
            }
        }
        return fileList;
    }

    @SuppressLint("SimpleDateFormat")
    private String dateCreate(File file){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        return dateFormat.format(file.lastModified());
    }

    private Double sizeFile(File file){
        double t = file.length();
        double t1 = t / 1024;
        double t2 = t1 / 1024;
        return t2;
    }

}
