package pdf_android.air.pdftest.file_module;

public class FileProvider {

    private DirectoryReader reader;
    private DirectoryWriter writer;

    public FileProvider(String path){
        reader = new DirectoryReader(path);
        writer = new DirectoryWriter(path);
    }

    public DirectoryWriter getWriter() {
        return writer;
    }

    public DirectoryReader getReader() {
        return reader;
    }
}
