package pdf_android.air.pdftest.file_module;

import android.graphics.pdf.PdfDocument;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DirectoryWriter {

    private String rootDirectory;

    DirectoryWriter(String path) {
        rootDirectory = path;
    }

    public void createNewFile(String name, PdfDocument document){
        File filePath = new File(rootDirectory + "/" + name + ".pdf");
        try {
            document.writeTo(new FileOutputStream(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.close();
    }

}
