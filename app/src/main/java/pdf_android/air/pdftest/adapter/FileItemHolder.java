package pdf_android.air.pdftest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import pdf_android.air.pdftest.R;

public class FileItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView sizeFile;
    public TextView nameFile;
    public TextView dateCreation;
    private ClickListener callback;

    FileItemHolder(View itemView, ClickListener callback) {
        super(itemView);
        this.callback = callback;
        sizeFile = itemView.findViewById(R.id.size_file);
        nameFile = itemView.findViewById(R.id.name_file);
        dateCreation = itemView.findViewById(R.id.date_creation);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        callback.onPositionClicked(getAdapterPosition());
    }

    public interface ClickListener {

        void onPositionClicked(int position);

    }

}
