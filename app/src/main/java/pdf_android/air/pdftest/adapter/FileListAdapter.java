package pdf_android.air.pdftest.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import pdf_android.air.pdftest.R;
import pdf_android.air.pdftest.model.FileEntity;

public class FileListAdapter extends RecyclerView.Adapter<FileItemHolder> {

    ArrayList<FileEntity> listData;
    FileItemHolder.ClickListener callback;

    public FileListAdapter(ArrayList<FileEntity> listData, FileItemHolder.ClickListener callback){
        this.listData = listData;
        this.callback = callback;
    }

    @NonNull
    @Override
    public FileItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FileItemHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.file_item, parent, false), callback);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull FileItemHolder holder, int position) {
        holder.sizeFile.setText(listData.get(position).sizeFile + "M");
        holder.nameFile.setText(listData.get(position).nameFile);
        holder.dateCreation.setText(listData.get(position).dateCreate);
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}
