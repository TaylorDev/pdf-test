package pdf_android.air.pdftest.view_model;

import android.arch.lifecycle.ViewModel;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;

import pdf_android.air.pdftest.file_module.DirectoryWriter;
import pdf_android.air.pdftest.file_module.FileProvider;

public class PdfCreatorViewModel extends ViewModel {

    private PdfDocument document = new PdfDocument();
    private int pageNumber = 1;
    private LoadCallback loadCallback;

    public void createPdfPage(Bitmap bitmap, Integer convertWidth, Integer convertHighet){

        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(convertWidth, convertHighet, pageNumber).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();


        Paint paint = new Paint();
        canvas.drawPaint(paint);


        bitmap = Bitmap.createScaledBitmap(bitmap, convertWidth, convertHighet, true);

        paint.setColor(Color.BLUE);
        canvas.drawBitmap(bitmap, 0, 0 , null);
        document.finishPage(page);
        pageNumber++;
    }

    public void createPdf(String name, FileProvider fileProvider){
        DirectoryWriter writer = fileProvider.getWriter();
        writer.createNewFile(name, document);
        document = new PdfDocument();
        pageNumber = 1;
        loadCallback.finishExport();
    }

    public void setCallback(LoadCallback loadCallback) {
        this.loadCallback = loadCallback;
    }

    public interface LoadCallback{
        void finishExport();
    }

}
