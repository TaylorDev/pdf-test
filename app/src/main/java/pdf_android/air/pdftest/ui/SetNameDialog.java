package pdf_android.air.pdftest.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import pdf_android.air.pdftest.R;

public class SetNameDialog extends DialogFragment implements View.OnClickListener{

    private SetNameCallback callback;
    private EditText name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.save_dialog, null);
        name = view.findViewById(R.id.name_file);
        view.findViewById(R.id.cancel_btn).setOnClickListener(this);
        view.findViewById(R.id.complit_btn).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.cancel_btn: {
                dismiss();
            }
            case R.id.complit_btn: {
                if (name.getText().toString().equals("")){
                    name.setError("Enter name");
                } else {
                    callback.getNameFile(name.getText().toString());
                    dismiss();
                }
            }
        }
    }

    public void setCallback(SetNameCallback callback){
        this.callback = callback;
    }

    interface SetNameCallback{
        void getNameFile(String name);
    }
}
