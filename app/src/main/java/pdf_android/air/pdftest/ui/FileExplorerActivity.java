package pdf_android.air.pdftest.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import pdf_android.air.pdftest.R;
import pdf_android.air.pdftest.adapter.FileItemHolder;
import pdf_android.air.pdftest.adapter.FileListAdapter;
import pdf_android.air.pdftest.file_module.DirectoryReader;
import pdf_android.air.pdftest.file_module.FileProvider;
import pdf_android.air.pdftest.model.FileEntity;

public class FileExplorerActivity extends AppCompatActivity {

    private FileProvider fileProvider;
    private TextView noFile;
    private RecyclerView pdfFileList;
    private ArrayList<FileEntity> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_explorer);
        noFile = findViewById(R.id.no_file);
        pdfFileList = findViewById(R.id.list_pdf_file);
        init();
    }

    private void init(){
        fileProvider = new FileProvider(getFilesDir().getPath());
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        DirectoryReader directoryReader = fileProvider.getReader();
        data = directoryReader.getFilesList();
        if(data.size() != 0){
            noFile.setVisibility(View.GONE);
            pdfFileList.setVisibility(View.VISIBLE);
            pdfFileList.setLayoutManager(new LinearLayoutManager(this));
            pdfFileList.setAdapter(new FileListAdapter(data, new FileItemHolder.ClickListener() {
                @Override
                public void onPositionClicked(int position) {
                    Intent intent = new Intent(getBaseContext(), PdfViewActivity.class);
                    intent.putExtra("FILE_NAME", data.get(position).nameFile);
                    intent.putExtra("FILE_PATH", data.get(position).pathToFile);
                    startActivity(intent);
                }
            }));
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
