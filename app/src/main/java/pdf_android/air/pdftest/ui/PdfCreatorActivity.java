package pdf_android.air.pdftest.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.Objects;

import pdf_android.air.pdftest.R;
import pdf_android.air.pdftest.file_module.FileProvider;
import pdf_android.air.pdftest.view_model.PdfCreatorViewModel;

public class PdfCreatorActivity extends AppCompatActivity implements SetNameDialog.SetNameCallback, PdfCreatorViewModel.LoadCallback {

    private FileProvider fileProvider;

    LinearLayout containerPdf;
    Button exportPdfBtn;
    Button loadImageBtn;
    Button addPageBtn;
    ProgressDialog progressDialog;
    Bitmap bitmap;
    EditText textData;
    ImageView imagePdf;
    PdfCreatorViewModel viewModel;
    Uri uri;

    private static final int REQUEST_PERMISSIONS = 109;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf_creator);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        fileProvider = new FileProvider(getFilesDir().getPath());
        init();
        if(!hasPermissions()){
            requestPerms();
        }

    }

    private void init() {
        viewModel = ViewModelProviders.of(this).get(PdfCreatorViewModel.class);
        viewModel.setCallback(this);
        textData = findViewById(R.id.text_data);
        containerPdf = findViewById(R.id.container_pdf);
        loadImageBtn = findViewById(R.id.load_image_btn);
        addPageBtn= findViewById(R.id.add_page_btn);
        exportPdfBtn = findViewById(R.id.export_pdf_btn);
        imagePdf = findViewById(R.id.image_pdf);
        visibleCursor();
        addPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invisibleCursor();
                addPageToDocument();
                setDefaultState();
                visibleCursor();
            }
        });
        loadImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
        exportPdfBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetNameDialog nameDialog= new SetNameDialog();
                nameDialog.setCallback(PdfCreatorActivity.this);
                nameDialog.show(getFragmentManager(), "nameDialog");
                invisibleCursor();
            }
        });
    }

    private void addPageToDocument(){
        bitmap = loadBitmapFromView(containerPdf, containerPdf.getWidth(), containerPdf.getHeight());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        float height = displaymetrics.heightPixels ;
        float width = displaymetrics.widthPixels ;

        int convertHeight = (int) height, convertWidth = (int) width;
        viewModel.createPdfPage(bitmap, convertWidth, convertHeight);
    }

    private void invisibleCursor() {
        Drawable drawable = textData.getBackground();
        drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        textData.setBackground(drawable);
        textData.setCursorVisible(false);
    }

    private void visibleCursor() {
        Drawable drawable = textData.getBackground();
        drawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        textData.setBackground(drawable);
        textData.setCursorVisible(true);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("text_pdf", textData.getText().toString());
        if (uri != null) {
            outState.putString("bitmapImage", uri.toString());
        } else {
            outState.putString("bitmapImage", "");
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        textData.setText(savedInstanceState.getString("text_pdf"));
        if(savedInstanceState.getString("bitmapImage") != "") {
            uri = Uri.parse(savedInstanceState.getString("bitmapImage"));
            Glide.with(this).load(uri).into(imagePdf);
        } else {
            Glide.with(this).load(R.drawable.no_image).into(imagePdf);
        }
        savedInstanceState.clear();
        super.onRestoreInstanceState(savedInstanceState);
    }

    private Bitmap loadBitmapFromView(View view, int width, int height) {
        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    @Override
    public void finishExport() {
        progressDialog.dismiss();
    }

    @Override
    public void getNameFile(String name) {
        progressDialog = new ProgressDialog(PdfCreatorActivity.this);
        progressDialog.setMessage("Please wait");
        progressDialog.show();
        addPageToDocument();
        viewModel.createPdf(name, fileProvider);
        setDefaultState();
        visibleCursor();
    }

    private void setDefaultState() {
        Glide.with(PdfCreatorActivity.this).load(R.drawable.no_image).into(imagePdf);
        textData.setText("");
        uri = null;
    }

    private boolean hasPermissions(){
        int res;
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        for (String perms : permissions){
            res = checkCallingOrSelfPermission(perms);
            if (!(res == PackageManager.PERMISSION_GRANTED)){
                return false;
            }
        }
        return true;
    }

    private void requestPerms(){
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permissions, REQUEST_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allowed = true;

        switch (requestCode){
            case REQUEST_PERMISSIONS:
                for (int res : grantResults){
                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (reqCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();
            Glide.with(this).load(uri).into(imagePdf);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}